#include "maf/random.hpp"

namespace maf {

namespace {

[[nodiscard]] unsigned get_seed_from_random_device() noexcept;

}

RandomContext::RandomContext(std::uniform_int_distribution<int> int_distribution,
                             std::uniform_real_distribution<double> real_distribution,
                             std::optional<unsigned> random_seed) noexcept
    : random_generator{random_seed ? *random_seed : get_seed_from_random_device()},
      int_distribution{int_distribution},
      real_distribution{real_distribution}
{}

template<>
int RandomContext::generate<int>() noexcept
{
    return int_distribution(random_generator);
}

template<>
double RandomContext::generate<double>() noexcept
{
    return real_distribution(random_generator);
}

namespace {

unsigned get_seed_from_random_device() noexcept
{
    static thread_local std::random_device random_device;
    return random_device();
}

} // namespace

} // namespace maf
