#ifndef MAF_LOG_HPP
#define MAF_LOG_HPP

#include "maf/observer.hpp"

namespace maf {

class ComponentLogObserver final : public ComponentObserver
{
  public:
    void on_accept_registered(const TcpEndpoint& listen_endpoint) noexcept final;
    void on_accept_success(const TcpEndpoint& peer_endpoint) noexcept final;
    void on_connection_from_unexpected_endpoint(const TcpEndpoint& peer_endpoint) noexcept final;
    void on_connection_identified(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept final;
    void on_multiple_connects_from_same_peer(const std::string& peer_name,
                                             const TcpEndpoint& peer_endpoint) noexcept final;

    void on_connect_registered(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept final;
    void on_connect_success(const std::string& peer_name, const TcpEndpoint& peer_endpoint) noexcept final;
};

} // namespace maf

#endif
