#ifndef MAF_OPTIONS_HPP
#define MAF_OPTIONS_HPP

#include "maf/configuration.hpp"

#include <boost/program_options.hpp>

#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace maf {

class ParsedCommandLineOptions final
{
  public:
    using BasicParsedOptions = boost::program_options::basic_parsed_options<char>;

  private:
    std::string executable_name;
    std::unordered_set<std::string> flag_options;
    std::unordered_map<std::string, std::string> argument_options;
    std::vector<std::string> positional_options;

  public:
    ParsedCommandLineOptions(BasicParsedOptions basic_parsed_options, std::string executable_name);

    [[nodiscard]] const std::string& get_executable_name() const noexcept;
    [[nodiscard]] bool is_flag_option_present(std::string_view key) const noexcept;
    [[nodiscard]] const std::string* get_argument_option_value(std::string_view key) const noexcept;
    [[nodiscard]] const std::vector<std::string>& get_positional_options() const noexcept;

  private:
    void add_flag_option(std::string key) noexcept;
    void add_argument_option(std::string key, std::string value) noexcept;
    void add_positional_option(std::string value) noexcept;
};

class CommandLineOptionsDescription final
{
  public:
    using ParsingError = boost::program_options::error;

  private:
    using OptionValue = boost::program_options::typed_value<std::string>;

  private:
    static constexpr unsigned help_line_length{120};

  private:
    boost::program_options::options_description options_description;

  public:
    CommandLineOptionsDescription() noexcept;

    [[nodiscard]] ParsedCommandLineOptions parse_command_line(int argc, char** argv) const;

    [[nodiscard]] std::string format_help_string(std::string_view executable_name) const noexcept;

  public:
    [[nodiscard]] static std::string format_usage_string(std::string_view executable_name) noexcept;
};

[[nodiscard]] DynamicConfigurationData
    load_dynamic_configuration_data(const ParsedCommandLineOptions& parsed_command_line_options);

class EmptyCommandLineError final : public std::runtime_error
{
  public:
    EmptyCommandLineError() noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyCommandLineError>);

class MultiTokenOptionEncounteredError final : public std::runtime_error
{
  public:
    explicit MultiTokenOptionEncounteredError(std::string_view key) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MultiTokenOptionEncounteredError>);

class InvalidObserverNameError final : public std::runtime_error
{
  public:
    InvalidObserverNameError(std::string_view observer_name,
                             const std::vector<std::string_view>& available_observers) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidObserverNameError>);

class InvalidSeedValueError final : public std::runtime_error
{
  public:
    explicit InvalidSeedValueError(std::string_view seed_value) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidSeedValueError>);

class InvalidConnectDelayValueError final : public std::runtime_error
{
  public:
    explicit InvalidConnectDelayValueError(std::string_view connect_delay_value) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidConnectDelayValueError>);

class InvalidIntDistributionRangeStringError final : public std::runtime_error
{
  public:
    explicit InvalidIntDistributionRangeStringError(std::string_view range_string) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidIntDistributionRangeStringError>);

class InvalidRealDistributionRangeStringError final : public std::runtime_error
{
  public:
    explicit InvalidRealDistributionRangeStringError(std::string_view range_string) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidRealDistributionRangeStringError>);

} // namespace maf

#endif
