#ifndef MAF_CONFIGURATION_HPP
#define MAF_CONFIGURATION_HPP

#include "maf/endpoint.hpp"
#include "maf/observer.hpp"

#include <array>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

namespace maf {

struct ComponentInfo final
{
    TcpEndpoint endpoint;
    std::string name;
};

struct StaticConfigurationData final
{
    std::string component_name;
    std::optional<TcpEndpoint> listen_endpoint;
    std::vector<ComponentInfo> connecting_components;
    std::vector<ComponentInfo> accepting_components;
};

struct DynamicConfigurationData final
{
    std::unique_ptr<ComponentObserver> component_observer{nullptr};

    std::optional<unsigned> random_seed;

    static constexpr std::chrono::milliseconds default_connect_delay{200};
    std::chrono::milliseconds connect_delay{default_connect_delay};

    static constexpr std::array<int, 2> default_int_distribution_range{-100000, 100000};
    static_assert(default_int_distribution_range[0] < default_int_distribution_range[1]);
    std::array<int, 2> int_distribution_range{default_int_distribution_range};

    static constexpr std::array<double, 2> default_real_distribution_range{-100000.0, 100000.0};
    static_assert(default_real_distribution_range[0] <= default_real_distribution_range[1]);
    std::array<double, 2> real_distribution_range{default_real_distribution_range};
};

class Configuration final
{
  private:
    StaticConfigurationData static_config_data;
    DynamicConfigurationData dynamic_config_data;

  public:
    Configuration(StaticConfigurationData static_config_data, DynamicConfigurationData dynamic_config_data);

    [[nodiscard]] const std::string& get_component_name() const noexcept;
    [[nodiscard]] const std::optional<TcpEndpoint>& get_listen_endpoint() const noexcept;
    [[nodiscard]] const std::vector<ComponentInfo>& get_connecting_components() const noexcept;
    [[nodiscard]] const std::vector<ComponentInfo>& get_accepting_components() const noexcept;
    [[nodiscard]] std::optional<unsigned> get_random_seed() const noexcept;
    [[nodiscard]] std::array<int, 2> get_int_distribution_range() const noexcept;
    [[nodiscard]] std::array<double, 2> get_real_distribution_range() const noexcept;
    [[nodiscard]] std::chrono::milliseconds get_connect_delay() const noexcept;
    [[nodiscard]] ComponentObserver* get_component_observer() const noexcept;

  private:
    void validate() const;
};

class EmptyComponentNameError final : public std::runtime_error
{
  public:
    explicit EmptyComponentNameError() noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyComponentNameError>);

class DuplicateComponentNameError final : public std::runtime_error
{
  public:
    explicit DuplicateComponentNameError(std::string_view duplicate_name) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<DuplicateComponentNameError>);

class DuplicateComponentEndpointError final : public std::runtime_error
{
  public:
    using TcpEndpoint = boost::asio::ip::tcp::endpoint;

  public:
    explicit DuplicateComponentEndpointError(const TcpEndpoint& duplicate_endpoint) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<DuplicateComponentEndpointError>);

class InvalidIntDistributionRangeError final : public std::runtime_error
{
  public:
    InvalidIntDistributionRangeError(int lower, int upper) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidIntDistributionRangeError>);

class InvalidRealDistributionRangeError final : public std::runtime_error
{
  public:
    InvalidRealDistributionRangeError(double lower, double upper) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidRealDistributionRangeError>);

class MissingListenEndpointError final : public std::runtime_error
{
  public:
    MissingListenEndpointError() noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingListenEndpointError>);

} // namespace maf

#endif
