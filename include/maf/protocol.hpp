#ifndef MAF_PROTOCOL_HPP
#define MAF_PROTOCOL_HPP

#include "maf/connection.hpp"
#include "maf/operation.hpp"
#include "maf/random.hpp"
#include "maf/serialization.hpp"

namespace maf::protocol {

namespace arithmetic {

template<typename ArithmeticOperation, typename Data>
void perform_initial(Data data,
                     Connection& second_component_connection,
                     Connection& final_component_connection,
                     RandomContext& random_context)
{
    static_assert(is_arithmetic_operand<Data>);
    static_assert(is_arithmetic_operation<ArithmeticOperation>);

    const auto mask           = random_context.generate<Data>();
    const auto initial_result = ArithmeticOperation{}(data, mask);
    second_component_connection.write(serialize(initial_result));
    final_component_connection.write(serialize(mask));
}

template<typename ArithmeticOperation, typename Data>
static void
    perform_intermediate(Data data, Connection& previous_component_connection, Connection& next_component_connection)
{
    static_assert(is_arithmetic_operand<Data>);
    static_assert(is_arithmetic_operation<ArithmeticOperation>);

    const auto previous_component_data = deserialize<Data>(previous_component_connection.read());
    const auto intermediate_result     = ArithmeticOperation{}(previous_component_data, data);
    next_component_connection.write(serialize(intermediate_result));
}

template<typename ArithmeticOperation, typename Data>
[[nodiscard]] static Data perform_final(Connection& initial_component_connection,
                                        Connection& previous_component_connection)
{
    static_assert(is_arithmetic_operation<ArithmeticOperation>);
    static_assert(is_arithmetic_operand<Data>);
    using InverseOperation = typename ArithmeticOperation::Inverse;
    static_assert(is_arithmetic_operation<InverseOperation>);

    const auto mask          = deserialize<Data>(initial_component_connection.read());
    const auto masked_result = deserialize<Data>(previous_component_connection.read());

    return InverseOperation{}(masked_result, mask);
}

} // namespace arithmetic

namespace comparison {

template<typename Data, typename MaskingOperation = Add>
void perform_initial(Data data,
                     Connection& second_component_connection,
                     Connection& final_component_connection,
                     RandomContext& random_context)
{
    static_assert(is_comparison_operand<Data>);
    static_assert(is_comparison_masking_operation<MaskingOperation>);

    const auto mask           = random_context.generate<Data>();
    const auto initial_result = MaskingOperation{}(data, mask);
    second_component_connection.write(serialize(mask));
    final_component_connection.write(serialize(initial_result));
}

template<typename Data, typename MaskingOperation = Add>
void perform_intermediate(Data data, Connection& initial_component_connection, Connection& final_component_connection)
{
    static_assert(is_comparison_operand<Data>);
    static_assert(is_comparison_masking_operation<MaskingOperation>);

    const auto mask                = deserialize<Data>(initial_component_connection.read());
    const auto intermediate_result = MaskingOperation{}(data, mask);
    final_component_connection.write(serialize(intermediate_result));
}

template<typename ComparisonOperation, typename Data>
[[nodiscard]] bool perform_final(Connection& initial_component_connection,
                                 Connection& intermediate_component_connection)
{
    static_assert(is_comparison_operand<Data>);
    static_assert(is_comparison_operation<ComparisonOperation>);

    const auto masked_initial_data      = deserialize<Data>(initial_component_connection.read());
    const auto masked_intermediate_data = deserialize<Data>(intermediate_component_connection.read());

    return ComparisonOperation{}(masked_initial_data, masked_intermediate_data);
}

} // namespace comparison

namespace equality {

// TODO: Change the hash function as the std::hash is not a cryptographic hash.

template<typename Data, typename Hash = std::hash<Data>, typename SaltOperation = Add>
void perform_initial(Data data,
                     Connection& second_component_connection,
                     Connection& final_component_connection,
                     RandomContext& random_context)
{
    static_assert(is_equality_operand<Data>);
    static_assert(is_salt_operation<SaltOperation>);
    static_assert(is_hash_operation<Hash, Data>);

    const auto salt           = random_context.generate<Data>();
    const auto initial_result = Hash{}(SaltOperation{}(data, salt));
    second_component_connection.write(serialize(salt));
    final_component_connection.write(serialize(initial_result));
}

template<typename Data, typename Hash = std::hash<Data>, typename SaltOperation = Add>
void perform_intermediate(Data data, Connection& initial_component_connection, Connection& final_component_connection)
{
    static_assert(is_equality_operand<Data>);
    static_assert(is_salt_operation<SaltOperation>);
    static_assert(is_hash_operation<Hash, Data>);

    const auto salt                = deserialize<Data>(initial_component_connection.read());
    const auto intermediate_result = Hash{}(SaltOperation{}(data, salt));
    final_component_connection.write(serialize(intermediate_result));
}

template<typename EqualityOperation>
[[nodiscard]] bool perform_final(Connection& initial_component_connection,
                                 Connection& intermediate_component_connection)
{
    static_assert(is_equality_operation<EqualityOperation>);

    const auto masked_initial_data      = deserialize<std::size_t>(initial_component_connection.read());
    const auto masked_intermediate_data = deserialize<std::size_t>(intermediate_component_connection.read());

    return EqualityOperation{}(masked_initial_data, masked_intermediate_data);
}

} // namespace equality

} // namespace maf::protocol

#endif
