project('maf', 'cpp',
        version: '0.4.0',
        default_options: ['cpp_std=c++17',
                          'warning_level=3',
                          'b_ndebug=if-release'])

maf_sources = files(
    'src/component.cpp',
    'src/configuration.cpp',
    'src/connection.cpp',
    'src/log.cpp',
    'src/options.cpp',
    'src/random.cpp',
)
maf_headers = files(
    'include/maf/component.hpp',
    'include/maf/configuration.hpp',
    'include/maf/connection.hpp',
    'include/maf/entry_point.hpp',
    'include/maf/log.hpp',
    'include/maf/observer.hpp',
    'include/maf/operation.hpp',
    'include/maf/options.hpp',
    'include/maf/protocol.hpp',
    'include/maf/random.hpp',
    'include/maf/serialization.hpp',
)
maf_include = include_directories('include')

boost_dep = dependency('boost', version: '>=1.75.0',
                       modules: ['serialization', 'program_options'])
fmt_dep   = dependency('fmt', version: '>=9.1.0')
maf_deps = [boost_dep, fmt_dep]

libmaf = static_library('maf', maf_sources,
                        extra_files: maf_headers,
                        include_directories: maf_include,
                        dependencies: maf_deps)

maf_dep = declare_dependency(include_directories: maf_include,
                             link_with: libmaf,
                             dependencies: maf_deps)

maf = executable('maf', 'src/main.cpp',
                 dependencies: maf_dep)

if get_option('test')
    subdir('test')
endif
