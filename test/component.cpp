#include <boost/test/unit_test.hpp>

#include "maf/component.hpp"

BOOST_AUTO_TEST_SUITE(component);

namespace {

class TestComponent final : maf::Component
{
  public:
    void on_all_connections_established() final
    {
        {
            perform_arithmetic_protocol_initial<Add>(10, "C1", "C2");
            perform_arithmetic_protocol_intermediate<Add>(10, "C0", "C2");
            [[maybe_unused]] const auto result = perform_arithmetic_protocol_final<Add, int>("C0", "C1");
        }

        {
            perform_comparison_protocol_initial(10, "C1", "C2");
            perform_comparison_protocol_intermediate(20, "C0", "C2");
            [[maybe_unused]] const auto result = perform_comparison_protocol_final<Greater, int>("C0", "C1");
        }

        {
            perform_equality_protocol_initial(10, "C1", "C2");
            perform_equality_protocol_intermediate(10, "C0", "C2");
            [[maybe_unused]] const auto result = perform_equality_protocol_final<Equal>("C0", "C1");
        }
    }
};

} // namespace

BOOST_AUTO_TEST_CASE(instantiate)
{}

BOOST_AUTO_TEST_SUITE_END();
